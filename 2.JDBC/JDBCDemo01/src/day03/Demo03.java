 package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Fetch Employee Based on EmpId
public class Demo03 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from Employee where empId = ?";
		
		System.out.print("Enter Employee Id: ");
		int empId = new java.util.Scanner(System.in).nextInt();
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setInt(1, empId);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble("salary"));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
			} else {
				System.out.println("Unable to Fetch Employee Record");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}











