 package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Dao.EmployeeDao;
import com.dto.Employee;


@WebServlet("/GetEmployeeById")
public class GetEmployeeById extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		int empId = Integer.parseInt(request.getParameter("empId"));

		EmployeeDao employeeDao = new EmployeeDao();
		Employee employee = employeeDao.getEmployeeById(empId);

		out.print("<body bgcolor='lightyellow' text='green'> <center>");	
				
		if (employee != null) {
			//adding the employee object under request object
			request.setAttribute("employee", employee);
				
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetEmployeeById.jsp");
			requestDispatcher.forward(request, response);
				
		} else {
				
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.include(request, response);
				
			out.print("<br/><h3 style='color:red;'>Unable to Fetch the Employee Record</h3>");
		}

		out.print("</center></body>");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}