 package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class EmployeeController {

	@Autowired
	EmployeeDao employeeDao;
	
	@GetMapping("getAllEmployees")
	public List<Employee> getAllEmployees() {
		return employeeDao.getAllEmployees();
	}
	
	@GetMapping("getEmployeeById/{empId}")
	public Employee getEmployeeById(@PathVariable("empId") int empId) {
		return employeeDao.getEmployeeById(empId);
	}
	
	@GetMapping("getEmployeeByName/{empName}")
	public Employee getEmployeeByName(@PathVariable("empName") String empName) {
		return employeeDao.getEmployeeByName(empName);
	}
	
	@PostMapping("addEmployee")
	public Employee addEmployee(@RequestBody Employee employee) {
		return employeeDao.addEmployee(employee);
	}
	
	@PutMapping("updateEmployee")
	public Employee updateEmployee(@RequestBody Employee employee) {
		return employeeDao.updateEmployee(employee);
	}
	
	@DeleteMapping("deleteEmployeeById/{empId}")
	public String deleteEmployeeById(@PathVariable("empId") int empId) {
		employeeDao.deleteEmployeeById(empId);
		return "Employee with EmpId: " + empId + ", Deleted Successfully!!!";
	}
	
	@GetMapping("empLogin/{emailId}/{password}")
	public Employee empLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		return employeeDao.empLogin(emailId, password);
	}
}













