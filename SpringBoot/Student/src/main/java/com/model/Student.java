 package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity	
public class Student {
	
	 @Id
	    @GeneratedValue
	    private int studentId;
	    private String studentName;
	    private int age;
	    private String gender;
	    private double fees;
	    private String emailId;
	    private String password;

	    @ManyToOne
	    @JoinColumn(name="courseId")
	    private Course course;
	
	
	public Student() {
			}
	public Student(int studentId, String studentName, int age, String gender, double fees,
			String emailId, String password) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.age = age;
		this.gender = gender;
		
		this.fees = fees;
		this.emailId = emailId;
		this.password = password;
	}
	public int getStudentId() {
		return studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public double getFees() {
		return fees;
	}
	public void setFees(double fees) {
		this.fees = fees;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", age=" + age + ", gender="
				+ gender + ", fees=" + fees + ", emailId=" + emailId + ", password=" + password + ", course=" + course
				+ "]";
	}
	
	
	

	
	
}