import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: boolean;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) { 
    this.loginStatus = false;
  }

  //Fetching all countries from rest countries api
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  //Employees API
  getAllEmployees(): any {
    return this.http.get('http://localhost:8085/getAllEmployees');
  }
  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }
  getAllDepartments(): any {
    return  this.http.get('http://localhost:8085/getAllDepartments');
  }
  registerEmplooyee(emp: any) {
    return this.http.post('http://localhost:8085/addEmployee', emp);
  }
  deleteEmployee(empId: any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }
  updateEmployee(emp: any) {
    return this.http.put('http://localhost:8085/updateEmployee', emp);
  }


  isUserLoggedIn() {
    this.loginStatus = true;
  }
  isUserLoggedOut() {
    this.loginStatus = false;
  }
  getLoginStatus(): boolean {
    return this.loginStatus;
  }
}

