import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {

  constructor(private router: Router, private service: EmpService) {
    alert('Logout Success!!!');
    this.service.isUserLoggedOut();
    this.router.navigate(['login']);
  }
}

