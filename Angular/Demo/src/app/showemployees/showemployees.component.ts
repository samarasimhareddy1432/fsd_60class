import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

declare var jQuery: any;

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {

  employees: any;
  emailId: any;
  
  //Following variables are used for edit employee
  countries: any;
  departments: any;
  editEmp: any;

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');

    //Defining the editEmp
    this.editEmp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId:''
      }
    }

  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => { this.employees = data; });

    //Fetching the data from the following api's
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }

  editEmployee(employee: any) {

    //Binding the Selected Record to the editEmp variable
    this.editEmp = employee;
    console.log(this.editEmp);

    jQuery('#empModal').modal('show');
  }

  //Updating the Modified Employee Data
  updateEmployee() {
    alert("Employee Record Updated...");
    console.log(this.editEmp);
    
    this.service.updateEmployee(this.editEmp).subscribe((data: any) => {
      console.log(data);
    });
  }

  deleteEmployee(empId:any) {
    this.service.deleteEmployee(empId).subscribe((data: any) => {console.log(data);});
  
    const i = this.employees.findIndex((element: any) => {
      return empId == element.empId;
    });

    this.employees.splice(i, 1);  
  }

}

